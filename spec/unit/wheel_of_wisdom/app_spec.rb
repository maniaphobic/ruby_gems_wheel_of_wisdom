require 'spec_helper'

describe 'WheelOfWisdom' do
  context '.spin' do
    it 'should return a string' do
      expect(WheelOfWisdom.spin).to be_a(String)
    end
  end

  context '.random_element' do
    it 'should return an element from its input' do
      input = %w[a b c]
      expect(
        WheelOfWisdom.random_element(input)
      ).to(satisfy { |e| input.include?(e) })
    end
  end
end
