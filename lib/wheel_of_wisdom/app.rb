require 'wheel_of_wisdom/lexicon'

# WheelOfWisdom
module WheelOfWisdom
  # Spin for wisdom
  # @return [String] wisdom
  def self.spin
    adjective = random_element(WheelOfWisdom::Adjectives)
    article = random_element(WheelOfWisdom::Articles)
    noun = random_element(WheelOfWisdom::Nouns)
    preface = random_element(WheelOfWisdom::Prefaces)
    prefix = random_element(WheelOfWisdom::Prefixes)
    verb = random_element(WheelOfWisdom::Verbs)
    "#{preface}, #{prefix} #{verb} #{article} #{adjective} #{noun}"
  end

  # Randomly select an element from the list
  #
  # @param list [Array<String>] a list of strings
  # @return [String] a randomly-chosen string
  def self.random_element(list)
    list[(list.length * rand).to_i]
  end
end
