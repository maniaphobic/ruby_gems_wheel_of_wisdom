#
module WheelOfWisdom
  Adjectives = %w[
    abstract
    efficient
    idempotent
    idiomatic
    immature
    immutable
    lexicographic
    mature
    modular
    pedagogical
    relevant
    significant
  ].freeze

  Adverbs = %w[
    incremental
    marginal
    nominal
    pragmatic
    statistical
  ].freeze

  Articles = %w[
    that
    the
  ].freeze

  Nouns = %w[
    API
    abstraction
    application
    contract
    cookbook
    dashboard
    data
    debt
    deployment
    friction
    function
    idiom
    implementation
    integration
    interface
    JIRA
    library
    metrics
    model
    pipeline
    recipe
    repository
    semantics
    service
    spike
    sprint
    story
    subtask
  ].freeze

  Verbs = %w[
    automate
    capture
    compose
    containerize
    converge
    decompose
    defer
    deploy
    distribute
    document
    implement
    increment
    instantiate
    integrate
    mature
    model
    mutate
    parameterize
    reify
    refactor
    scale
    stabilize
    templatize
    wikify
  ].freeze

  Prefaces = [
    'As I\'ve said many times',
    'As a meta-comment',
    'Good idea',
    'Grr',
    'Hmm',
    'Hold on',
    'I have an idea',
    'Once again',
    '*Sigh*'
  ].freeze

  Prefixes = [
    'I propose we',
    'kindly',
    'let\'s',
    'please',
    'we need to',
    'we should'
  ].freeze

  Suffixes = %w[
  ].freeze

  #
  class Phrase
  end

  #
  class Clause < Phrase
  end

  #
  class PrepositionalPhrase < Phrase
  end

  #
  class Sentence
  end
end
