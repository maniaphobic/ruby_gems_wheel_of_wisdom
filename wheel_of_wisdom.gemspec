$LOAD_PATH.unshift('lib')
require 'wheel_of_wisdom'

gems = {
  development: {
    'rubocop' => '~> 0',
    'rspec' => '~> 0'
  },
  runtime: {
  }
}

Gem::Specification.new do |spec|
  gems[:development].each do |gem_name, gem_version|
    add_development_gems gem_name, gem_version
  end
  gems[:runtime].each do |gem_name, gem_version|
    add_runtime_gems gem_name, gem_version
  end
  spec.authors     = 'Some One'
  spec.description = File.read('README.md')
  spec.email       = 'me@myself.i'
  spec.files       = Dir.glob('lib/**/*.rb')
  spec.homepage    = 'https://me.myself.i'
  spec.licenses    = %w[0BSD]
  spec.name        = 'wheel_of_wisdom'
  spec.summary     = 'Spin for wisdom'
  spec.version     = WheelOfWisdom::VERSION
end
